<?php
/*
* fordela_video_video cck definition
*/
function _fordela_cck_export() {
	$content['type']  = array (
	  'name' => 'Video',
	  'type' => 'fordela_video',
	  'description' => '',
	  'title_label' => 'Title',
	  'body_label' => 'Body',
	  'min_word_count' => '0',
	  'help' => '',
	  'node_options' => 
	  array (
	    'status' => true,
	    'promote' => true,
	    'sticky' => false,
	    'revision' => false,
	  ),
	  'old_type' => 'fordela_video',
	  'orig_type' => '',
	  'module' => 'node',
	  'custom' => '1',
	  'modified' => '1',
	  'locked' => '0',
	  'comment' => '0',
	  'comment_default_mode' => '4',
	  'comment_default_order' => '1',
	  'comment_default_per_page' => '50',
	  'comment_controls' => '3',
	  'comment_anonymous' => 0,
	  'comment_subject_field' => '1',
	  'comment_preview' => '1',
	  'comment_form_location' => '0',
	);
	$content['fields']  = array (
	  0 => 
	  array (
	    'label' => 'Media ID',
	    'field_name' => 'field_fordela_video_mediaid',
	    'type' => 'number_integer',
	    'widget_type' => 'number',
	    'change' => 'Change basic information',
	    'weight' => '-4',
	    'description' => 'Media ID is the same as the Fordela Platform Video ID.',
	    'default_value' => 
	    array (
	      0 => 
	      array (
	        'value' => '',
	        '_error_element' => 'default_value_widget][field_fordela_video_mediaid][0][value',
	      ),
	    ),
	    'default_value_php' => '',
	    'default_value_widget' => NULL,
	    'group' => false,
	    'required' => 1,
	    'multiple' => '0',
	    'min' => '',
	    'max' => '',
	    'prefix' => '',
	    'suffix' => '',
	    'allowed_values' => '',
	    'allowed_values_php' => '',
	    'op' => 'Save field settings',
	    'module' => 'number',
	    'widget_module' => 'number',
	    'columns' => 
	    array (
	      'value' => 
	      array (
	        'type' => 'int',
	        'not null' => false,
	        'sortable' => true,
	      ),
	    ),
	    'display_settings' => 
	    array (
	      'weight' => '-4',
	      'parent' => '',
	      'label' => 
	      array (
	        'format' => 'inline',
	      ),
	      'teaser' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'full' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      4 => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	    ),
	  ),
	  1 => 
	  array (
	    'label' => 'Version ID',
	    'field_name' => 'field_fordela_video_version_id',
	    'type' => 'number_integer',
	    'widget_type' => 'number',
	    'change' => 'Change basic information',
	    'weight' => '-3',
	    'description' => '',
	    'default_value' => 
	    array (
	      0 => 
	      array (
	        'value' => '',
	        '_error_element' => 'default_value_widget][field_fordela_video_version_id][0][value',
	      ),
	    ),
	    'default_value_php' => '',
	    'default_value_widget' => 
	    array (
	      'field_fordela_video_version_id' => 
	      array (
	        0 => 
	        array (
	          'value' => '',
	          '_error_element' => 'default_value_widget][field_fordela_video_version_id][0][value',
	        ),
	      ),
	    ),
	    'group' => false,
	    'required' => 0,
	    'multiple' => '0',
	    'min' => '',
	    'max' => '',
	    'prefix' => '',
	    'suffix' => '',
	    'allowed_values' => '',
	    'allowed_values_php' => '',
	    'op' => 'Save field settings',
	    'module' => 'number',
	    'widget_module' => 'number',
	    'columns' => 
	    array (
	      'value' => 
	      array (
	        'type' => 'int',
	        'not null' => false,
	        'sortable' => true,
	      ),
	    ),
	    'display_settings' => 
	    array (
	      'weight' => '-3',
	      'parent' => '',
	      'label' => 
	      array (
	        'format' => 'inline',
	      ),
	      'teaser' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'full' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      4 => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	    ),
	  ),
	  2 => 
	  array (
	    'label' => 'Preview Image',
	    'field_name' => 'field_fordela_video_image',
	    'type' => 'filefield',
	    'widget_type' => 'imagefield_widget',
	    'change' => 'Change basic information',
	    'weight' => '-2',
	    'file_extensions' => 'png gif jpg jpeg',
	    'progress_indicator' => 'bar',
	    'file_path' => '',
	    'max_filesize_per_file' => '',
	    'max_filesize_per_node' => '',
	    'max_resolution' => 0,
	    'min_resolution' => 0,
	    'custom_alt' => 0,
	    'alt' => '',
	    'custom_title' => 0,
	    'title_type' => 'textfield',
	    'title' => '',
	    'use_default_image' => 0,
	    'default_image_upload' => '',
	    'default_image' => NULL,
	    'description' => '',
	    'group' => false,
	    'required' => 0,
	    'multiple' => '0',
	    'list_field' => '0',
	    'list_default' => 1,
	    'description_field' => '0',
	    'op' => 'Save field settings',
	    'module' => 'filefield',
	    'widget_module' => 'imagefield',
	    'columns' => 
	    array (
	      'fid' => 
	      array (
	        'type' => 'int',
	        'not null' => false,
	        'views' => true,
	      ),
	      'list' => 
	      array (
	        'type' => 'int',
	        'size' => 'tiny',
	        'not null' => false,
	        'views' => true,
	      ),
	      'data' => 
	      array (
	        'type' => 'text',
	        'serialize' => true,
	        'views' => true,
	      ),
	    ),
	    'display_settings' => 
	    array (
	      'weight' => '-2',
	      'parent' => '',
	      'label' => 
	      array (
	        'format' => 'above',
	      ),
	      'teaser' => 
	      array (
	        'format' => '600px_wide_linked',
	        'exclude' => 0,
	      ),
	      'full' => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 1,
	      ),
	      4 => 
	      array (
	        'format' => 'image_plain',
	        'exclude' => 0,
	      ),
	    ),
	  ),
	  3 => 
	  array (
	    'label' => 'Runtime',
	    'field_name' => 'field_fordela_video_runtime',
	    'type' => 'text',
	    'widget_type' => 'text_textfield',
	    'change' => 'Change basic information',
	    'weight' => '-1',
	    'rows' => 5,
	    'size' => '60',
	    'description' => '',
	    'default_value' => 
	    array (
	      0 => 
	      array (
	        'value' => '',
	        '_error_element' => 'default_value_widget][field_fordela_video_runtime][0][value',
	      ),
	    ),
	    'default_value_php' => '',
	    'default_value_widget' => 
	    array (
	      'field_fordela_video_runtime' => 
	      array (
	        0 => 
	        array (
	          'value' => '',
	          '_error_element' => 'default_value_widget][field_fordela_video_runtime][0][value',
	        ),
	      ),
	    ),
	    'group' => false,
	    'required' => 0,
	    'multiple' => '0',
	    'text_processing' => '0',
	    'max_length' => '',
	    'allowed_values' => '',
	    'allowed_values_php' => '',
	    'op' => 'Save field settings',
	    'module' => 'text',
	    'widget_module' => 'text',
	    'columns' => 
	    array (
	      'value' => 
	      array (
	        'type' => 'text',
	        'size' => 'big',
	        'not null' => false,
	        'sortable' => true,
	        'views' => true,
	      ),
	    ),
	    'display_settings' => 
	    array (
	      'weight' => '-1',
	      'parent' => '',
	      'label' => 
	      array (
	        'format' => 'inline',
	      ),
	      'teaser' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'full' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      4 => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	    ),
	  ),
	);
	$content['extra']  = array (
	  'title' => '-5',
	  'body_field' => '0',
	  'revision_information' => '3',
	  'author' => '1',
	  'options' => '4',
	  'comment_settings' => '5',
	  'menu' => '2',
	);
	
	return $content;
}
