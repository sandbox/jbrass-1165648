<div<?php print $article_id ? ' id="'. $article_id .'"' : ''; ?> class="<?php print $classes; ?>">

  <?php if (!$page): ?>
    <h2<?php print $title_classes ? ' class="'. $title_classes .'"' : ''; ?>>
      <a href="<?php print $node_url; ?>" rel="bookmark"><?php print $title; ?></a>
      <?php print $unpublished; ?>
    </h2>
  <?php endif; ?>

  <?php if ($submitted): ?>
    <p class="submitted"><?php print $submitted; ?></p>
  <?php endif; ?>

  <div id="content">
  <?php
		if ($teaser) {
			// Do not show embed code
	  	} 
		else { 
			// Embed code
			fordela_player($node);
		}
	
		echo '<br ><br >';

		//print $content;  
		echo $node->body;
  ?>
  </div>

  <!-- <pre><?php //print_r($node); ?></pre> -->

  <?php if ($terms): print $terms; endif; ?>

  <?php if ($links): print $links; endif; ?>

  <?php if ($article_aside && !$teaser): ?>
    <div id="article-aside" class="aside"><?php print $article_aside; ?></div>
  <?php endif; ?>

</div> <!-- /article -->
