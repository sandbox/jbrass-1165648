<?php
class FordelaVideoNode
{
	//var $id;
	//var $type;
	var $title;
	var $name;
	var $created;
	var $body;
	var $format;
	var $status;
	var $taxonomy;
	//var $_vms_image;
	var $_vms_image = 'field_fordela_video_image';
	
	
	function __construct($item = array(),$node = null)
	{
		if(empty($item['Video']))
		{
			trigger_error('Please Submit a properly formatted VMS Video.');
			return false;
		}
		
		if(is_object($node)){
			$this->loadFromNode($node);
		}
		
		
		$image = fordela_get_image($item);

		if($image){
			$this->{$this->_vms_image} = array(fordela_drupal_add_existing_file($image));
		}
		
		$this->title = $item['Video']['title'];
		$this->name = $item['Video']['title'];
		$this->created = strtotime($item['Video']['created']);
		$this->body = $item['Video']['description'];
		$this->format = 2;  // 1:Filtered HTML, 2: Full HTML
		$this->status = 1;
		$this->comment = 1;
		$this->uid = 1; // sets authored by to admin
		
		$this->type = 'fordela_video';
		$this->field_fordela_video_mediaid[0]['value'] = $item['PlaylistItem']['foreign_id'];
		$this->field_fordela_video_version_id[0]['value'] = $item['PlaylistItem']['version_id'];
		$this->field_fordela_video_runtime[0]['value'] = $item['Video']['runtime'];
		
		$this->taxonomy = fordela_get_taxonomy($item);
	}
	
	
    function loadFromNode( $node )
    {
		$fields = get_object_vars( $node );
		foreach($fields as $k=>$v){
			$this->{$k} = $v;
		}
    }
	
}
